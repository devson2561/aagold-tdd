import axios from "axios";

const mock_data = {
  count: 6,
  next: null,
  previous: null,
  results: [
    {
      code: "R000008",
      name: "นาฬิกาข้อมืออัจฉริยะกันน้ำสำหรับ Android IOS",
      image: {
        full_size:
          "https://mapi-stg.aagold-th.com/media/aag_api/reward/47dc9fe8543847913b32e1bfb03c91c0_enyOz1R.jpeg",
        crop:
          "https://mapi-stg.aagold-th.com/media/__sized__/aag_api/reward/47dc9fe8543847913b32e1bfb03c91c0_enyOz1R-crop-c0-5__0-5-300x300-70.jpeg"
      },
      score: 650,
      created_datetime: "2018-12-13T12:20:21.771164+07:00",
      id: 6
    },
    {
      code: "R000007",
      name: "นาฬิกาแฟชั่นผู้ชายนาฬิกาข้อมือควอตซ์นาฬิกาข้อมือกีฬาสร้างสรรค์",
      image: {
        full_size:
          "https://mapi-stg.aagold-th.com/media/aag_api/reward/f9bb177e00f00ce63958bba5a39d364d.jpeg",
        crop:
          "https://mapi-stg.aagold-th.com/media/__sized__/aag_api/reward/f9bb177e00f00ce63958bba5a39d364d-crop-c0-5__0-5-300x300-70.jpeg"
      },
      score: 120,
      created_datetime: "2018-12-13T12:11:36.286651+07:00",
      id: 5
    },
    {
      code: "R000006",
      name: "นาฬิกาผู้ชาย สาย Stainless ของแท้ ประกัน",
      image: {
        full_size:
          "https://mapi-stg.aagold-th.com/media/aag_api/reward/47dc9fe8543847913b32e1bfb03c91c0.jpeg",
        crop:
          "https://mapi-stg.aagold-th.com/media/__sized__/aag_api/reward/47dc9fe8543847913b32e1bfb03c91c0-crop-c0-5__0-5-300x300-70.jpeg"
      },
      score: 250,
      created_datetime: "2018-12-13T12:09:28.898145+07:00",
      id: 4
    },
    {
      code: "R000005",
      name: "นาฬิกา ควอทซ์ สแตนเลส หรูหรา กันน้ำ สำหรับผู้ชาย + กล่อง",
      image: {
        full_size:
          "https://mapi-stg.aagold-th.com/media/aag_api/reward/8f03fa110ab81b996826af0a2269b366.jpeg",
        crop:
          "https://mapi-stg.aagold-th.com/media/__sized__/aag_api/reward/8f03fa110ab81b996826af0a2269b366-crop-c0-5__0-5-300x300-70.jpeg"
      },
      score: 150,
      created_datetime: "2018-12-13T12:08:46.729399+07:00",
      id: 3
    },
    {
      code: "R000004",
      name: "BOSCKชายและหญิงแฟชั่นธุรกิจนาฬิกาควอตซ์กันน้ำ",
      image: {
        full_size:
          "https://mapi-stg.aagold-th.com/media/aag_api/reward/893681bb225a1bf454ca15608414b87d.jpeg",
        crop:
          "https://mapi-stg.aagold-th.com/media/__sized__/aag_api/reward/893681bb225a1bf454ca15608414b87d-crop-c0-5__0-5-300x300-70.jpeg"
      },
      score: 2497,
      created_datetime: "2018-12-13T12:07:40.507052+07:00",
      id: 2
    },
    {
      code: "R000003",
      name: "นาฬิกาข้อมือผู้ชาย - GP9138 (Silver/Black)",
      image: {
        full_size:
          "https://mapi-stg.aagold-th.com/media/aag_api/reward/337d683ea975f3c72a43827459954f9e_8cBpMuu.jpeg",
        crop:
          "https://mapi-stg.aagold-th.com/media/__sized__/aag_api/reward/337d683ea975f3c72a43827459954f9e_8cBpMuu-crop-c0-5__0-5-300x300-70.jpeg"
      },
      score: 1200,
      created_datetime: "2018-12-13T12:06:29.603768+07:00",
      id: 1
    }
  ]
};

const list = async () => {
  try {
    // const res = await axios.get("https://mapi-stg.aagold-th.com/v1/reward");
    return {
      data: mock_data,
      status: 200
    };
  } catch (error) {
    throw error;
  }
};

const get = async id => {
  try {
    const res = await axios.get(
      `https://mapi-stg.aagold-th.com/v1/reward/${id}`
    );
    return res;
  } catch (error) {
    throw error;
  }
};

export default {
  list,
  get
};
