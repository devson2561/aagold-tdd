import React from "react";
import { shallow, render, mount } from "enzyme";

import Service from "../services/reward.service";
import RewardsPage from "../pages/Rewards.page";

import "./setupTests";

describe("Rewards Service", () => {
  it("can fetch rewards", async () => {
    const res = await Service.list();
    expect(res.status).toEqual(200);
  });

  it("can fetch reward by id", async () => {
    const res = await Service.get(1);
    expect(res.status).toEqual(200);
  });
});

describe("Rewards Page", () => {
  it("should render reward item equal reward length", async () => {
    const {
      data: { results: rewards }
    } = await Service.list();
    const wrapper = render(<RewardsPage rewards={rewards} />);
    expect(wrapper.find('.card')).toHaveLength(rewards.length);
  });
});
