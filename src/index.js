import React from "react";
import ReactDOM from "react-dom";
import axios from "axios";

import 'bulma/css/bulma.css'
import "./index.css";
import App from "./App";
import * as serviceWorker from "./serviceWorker";

axios.defaults.baseURL = "https://mapi-stg.aagold-th.com/v1";
axios.defaults.headers.common["Authorization"] =
  "Bearer BEPs7GfBMYDKrJq7vuhhC8RXxSjws";

ReactDOM.render(<App />, document.getElementById("root"));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: http://bit.ly/CRA-PWA
serviceWorker.unregister();
