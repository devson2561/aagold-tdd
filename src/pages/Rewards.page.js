import React, { Component } from "react";

import RewardService from "../services/reward.service";
import RewardListItem from "../components/RewardListItem.component";

class RewardsPage extends Component {
  renderRewards = rewards => {
    return rewards.map((reward, index) => {
      return <RewardListItem data={reward} key={index} />;
    });
  };

  render() {
    const { rewards } = this.props;
    return (
      <section className="section">
        <div className="container">
          <h2 className="title">Rewards</h2>
          {this.renderRewards(rewards)}
        </div>
      </section>
    );
  }
}

RewardsPage.defaultProps = {
  rewards: []
};

export default RewardsPage;
