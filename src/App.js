import React, { Component } from "react";

import RewardsContainer from "./containers/Rewards.container";
import "./App.css";

class App extends Component {
  render() {
    return <RewardsContainer />;
  }
}

export default App;
