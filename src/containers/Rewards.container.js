import React, { Component } from "react";

import RewardService from "../services/reward.service";
import RewardsPage from "../pages/Rewards.page";

export default class RewardsContainer extends Component {
  state = {
    rewards: []
  };
  componentDidMount = () => {
    this.fetchRewards();
  };

  fetchRewards = async () => {
    try {
      const { data } = await RewardService.list();
      const { results } = data;
      this.setState({
        rewards: results
      });
    } catch (error) {}
  };

  render() {
    const { rewards } = this.state;
    console.log(rewards, "rewards..");
    return <RewardsPage rewards={rewards} />;
  }
}
