import React from "react";
import PropTypes from "prop-types";

const RewardListItem = props => {
  const {
    name,
    code,
    score,
    image: { crop }
  } = props.data;
  return (
    <div className="card reward-card">
      <div className="card-body">
        <img src={crop} alt="" />
        <br />
        <h3 className="title reward-name">{name}</h3>
        <h3 className="title reward-code"> {code}</h3>
        <h3 className="title point">{score} points</h3>
      </div>
    </div>
  );
};

RewardListItem.defaultProps = {
  id: null,
  code: "",
  name: "",
  image: {
    full_size: "",
    crop: ""
  },
  score: 0,
  created_datetime: null
};

RewardListItem.propTypes = {
  data: PropTypes.object.isRequired
};

export default RewardListItem;
